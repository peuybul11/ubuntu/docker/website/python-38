FROM python:3.8-slim

COPY . .
WORKDIR /app

COPY .config/requirements.txt requirements.txt
RUN pip install -r requirements.txt

EXPOSE 5000

CMD ["python", "app.py"]
