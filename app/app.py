from flask import Flask, render_template
import mysql.connector

app = Flask(__name__)

db = mysql.connector.connect(
    host="db",  
    user="your_user",
    password="your_password",
    database="your_database"
)

cursor = db.cursor()

@app.route('/')
def index():
    cursor.execute("SELECT * FROM your_table")
    data = cursor.fetchall()
    return render_template('index.html', data=data)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
